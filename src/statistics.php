<?php

require_once 'header.php';

$connection = getConnection();

if (!isLogged() || !isSessionAdmin($connection)) {
    closeConnexion($connection);
    header("Location: index.php");
}

$users = getUsers($connection);
$picturesByCategory = getPicturesByCategory($connection);
$picturesByUser = getPicturesOrderedByUser($connection);

closeConnexion($connection);
?>

<html lang="fr">
<head>
    <meta charset="utf-8">

    <title>Pinterest - Statistiques</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
          crossorigin="anonymous">
</head>

<body>
<div class="container">
    <?php
    $currentPage = "Statistics";
    include 'nav.php';
    ?>

    <h1>Statistiques du site</h1>

    <p>
        Il y a <span class="fw-bold"><?= count($users) ?></span>
        utilisateur(s)
        inscrit(s)
        <br>
    </p>

    <p>Photos par utilisateur :</p>

    <ul>
        <?php
        foreach ($picturesByUser as $pictures) {
            ?>
            <li>
                <a href="editUser.php?id=<?= $pictures['userId'] ?>"><?=
                    $pictures['userPseudo'] ?></a> a
                posté <?=
                $pictures['count']
                ?> photo(s)
            </li>
            <?php
        }
        ?>
    </ul>

    <p>Photos par catégorie :</p>

    <ul>
        <?php
        foreach ($picturesByCategory as $pictures) {
            $category = $pictures['category'];
            ?>
            <li>
                <?= $pictures['count'] ?> photo(s) dans la
                catégorie <a href="index.php?category=<?= $category ?>"><?=
                    $category ?></a>
            </li>
            <?php
        }
        ?>
    </ul>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
            crossorigin="anonymous"></script>
</body>
</html>
