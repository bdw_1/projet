<?php

require_once 'header.php';

$connection = getConnection();

if (isLogged()) {
    ?>
    <p class="text-end">Connecté en tant que <?= getSessionPseudo(
            $connection
        ) ?> depuis
        <?php
        $time = time() - $_SESSION['openTime'];
        echo date("i", $time) . " minute(s) et " . date(
                "s",
                $time
            ) . " seconde(s)";
        ?>
    </p>
    <?php
}
?>
    <!-- Menu de navigation -->
    <div class="mt-3 mb-4">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link<?php
                if (isset($currentPage) && $currentPage == "Index") echo " active" ?>"
                   aria-current="page" href="index.php">Liste des photos</a>
            </li>
            <?php
            if (isLogged()) {
                ?>
                <li class="nav-item">
                    <a class="nav-link<?php
                    if (isset($currentPage) && $currentPage == "AddPicture") echo " active" ?>"
                       href="addPicture.php">Ajouter une photo</a>
                </li>
                <?php
                if (isSessionAdmin($connection)) {
                    ?>
                    <li class="nav-item">
                        <a class="nav-link<?php
                        if (isset($currentPage) && ($currentPage == "Users" || $currentPage == "EditUser")) echo " active" ?>"
                           href="users.php">Utilisateurs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link<?php
                        if (isset($currentPage) && $currentPage == "Statistics") echo " active" ?>"
                           href="statistics.php">Statistiques</a>
                    </li>
                    <?php
                }
            }
            ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle<?php
                if (isset($currentPage) && ($currentPage == "Profile" || $currentPage == "EditPicture" || $currentPage == "Registration" || $currentPage == "Connection")) echo " active" ?>"
                   data-bs-toggle="dropdown" href="" role="button"
                   aria-expanded="false"><?= isLogged(
                    ) ? "Profil" : "Connexion" ?></a>
                <ul class="dropdown-menu">
                    <?php
                    if (isLogged()) {
                        ?>
                        <li><a class="dropdown-item" href="profile.php">Mon
                                profil</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="disconnection.php">Se
                                déconnecter</a></li>
                        <?php
                    } else {
                        ?>
                        <li><a class="dropdown-item" href="connection.php">Se
                                connecter</a></li>
                        <li><a class="dropdown-item" href="registration.php">S'inscrire</a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </li>
        </ul>
    </div>

<?php
closeConnexion($connection);