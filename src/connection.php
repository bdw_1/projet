<?php

require_once 'header.php';

if (isLogged()) {
    header(
        'Location: index.php'
    ); // Si l'utilisateur est déjà connecté, on le redirige vers la page d'accueil
}

if (!empty($_POST)) {
    $connection = getConnection();

    $pseudo = $_POST['pseudo'] ?? null;

    if (!$pseudo) {
        $errors[] = "Vous devez saisir un pseudo";
    } else {
        if (!pseudoExists($connection, $pseudo)) {
            $errors[] = "Le pseudo que vous avez saisi n'existe pas";
        } else {
            $user = getUserByPseudo($connection, $pseudo);

            $password = $_POST['password'] ?? null;
            if (!$password) {
                $errors[] = "Vous devez saisir un mot de passe";
            } else {
                if (!password_verify($password, $user['password'])) {
                    $errors[] = "Le mot de passe que vous avez saisi n'est pas le bon";
                }
            }

            if (!isset($errors)) {
                // Connexion
                connectUser($user['id']);

                closeConnexion($connection);

                setFlash("Vous êtes désormais connecté");
                header('Location: index.php'); // Redirection à l'accueil
            }
        }
    }

    closeConnexion($connection);
}
?>

<html lang="fr">
<head>
    <meta charset="utf-8">

    <title>Pinterest - Connexion</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
          crossorigin="anonymous">
</head>

<body>
<div class="container">
    <?php
    $currentPage = "Connection";
    include 'nav.php';
    ?>

    <h1>Connexion</h1>

    <?php
    if (isset($errors)) {
        printFormErrors($errors);
    }
    ?>

    <form action="connection.php" method="post">
        <div class="form-floating mb-3">
            <input id="pseudo" name="pseudo" type="text" class="form-control">
            <label for="pseudo" class="form-label">Pseudo</label>
        </div>

        <div class="form-floating mb-3">
            <input id="password" name="password" type="password"
                   class="form-control">
            <label for="password" class="form-label">Mot de passe</label>
        </div>

        <button type="submit" class="btn btn-primary">Se connecter</button>
    </form>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
        crossorigin="anonymous"></script>

</body>
</html>