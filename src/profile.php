<?php

require_once 'header.php';

if (!isLogged()) {
    header(
        "Location: index.php"
    ); // Si l'utilisateur n'est pas connecté, on le redirige vers la page d'accueil
}

$subPage = $_GET['page'] ?? "credentials";

$connection = getConnection();

$pictures = getPicturesFromUser($connection, getSessionId());

$currentPseudo = getSessionPseudo($connection);
$currentPassword = getSessionPassword($connection);

if (!empty($_POST)) {
    $newPseudo = $_POST['pseudo'] ?? null;
    $newPassword = $_POST['password'] ?? null;

    $errors = checkUserForm(
        $connection,
        $newPseudo,
        $newPassword,
        $currentPseudo
    );

    if (empty($errors)) {
        updateUserPseudo($connection, getSessionId(), $newPseudo);
        $currentPseudo = $newPseudo;

        if ($newPassword !== $currentPassword) {
            $hashedPassword = password_hash($newPassword, PASSWORD_DEFAULT);
            updateUserPassword($connection, getSessionId(), $hashedPassword);
            $currentPassword = $hashedPassword;
        }

        setFlash("Vos données ont bien été sauvegardées");
    }
}

closeConnexion($connection);
?>

<html lang="fr">
<head>
    <meta charset="utf-8">

    <title>Pinterest - Profil</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
          crossorigin="anonymous">
</head>

<body>
<div class="container">
    <?php
    $currentPage = "Profile";
    include 'nav.php';
    ?>

    <h1>Mon profil</h1>

    <?php
    printFlash();
    ?>

    <?php
    if (isset($errors)) {
        printFormErrors($errors);
    }
    ?>

    <div class="mt-3 d-flex align-items-start">
        <div class="nav flex-column nav-pills me-3" id="v-pills-tab"
             role="tablist" aria-orientation="vertical">
            <button class="nav-link<?php
            if ($subPage === "credentials") {
                echo " active";
            } ?>"
                    id="v-pills-credentials-tab" data-bs-toggle="pill"
                    data-bs-target="#v-pills-credentials" type="button"
                    role="tab" aria-controls="v-pills-credentials"
                    aria-selected="true">Identifiants
            </button>

            <button class="nav-link<?php
            if ($subPage === "photos") {
                echo " active";
            } ?>"
                    id="v-pills-pictures-tab" data-bs-toggle="pill"
                    data-bs-target="#v-pills-pictures"
                    type="button" role="tab" aria-controls="v-pills-pictures"
                    aria-selected="false">Mes photos
            </button>
        </div>
        <div class="tab-content" id="v-pills-tabContent">
            <div id="v-pills-credentials" class="tab-pane fade<?php
            if ($subPage === "credentials") {
                echo " show active";
            } ?>"
                 role="tabpanel"
                 aria-labelledby="v-pills-home-tab">
                <h2>Identifiants</h2>

                <form action="profile.php" method="post">
                    <div class="form-floating mb-3">
                        <input id="pseudo" name="pseudo" type="text"
                               value="<?= $currentPseudo ?>"
                               class="form-control">
                        <label for="pseudo" class="form-label">Changer de
                            pseudo</label>
                    </div>

                    <div class="form-floating mb-3">
                        <input id="password" name="password" type="password"
                               class="form-control"
                               value="<?= $currentPassword ?>">
                        <label for="password" class="form-label">Changer de mot
                            de passe</label>
                        <div id="passwordHelp" class="form-text">Une
                            version cryptée de votre mot de passe est
                            affichée</div>
                    </div>

                    <button type="submit" class="btn btn-primary">Sauvegarder
                    </button>
                </form>
            </div>

            <div id="v-pills-pictures" class="tab-pane fade<?php
            if ($subPage === "photos") {
                echo " show active";
            } ?>" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                <h3>Mes Photos</h3>

                <?php
                if (empty($pictures)) {
                    ?>
                    <p>Aucune photo à afficher</p>
                    <?php
                } else {
                    ?>
                    <div class="row">
                        <?php
                        foreach ($pictures as $picture) {
                            ?>
                            <div class="col-lg-3 col-md-4 col-6">
                                <a href="editPicture.php?id=<?= $picture['id'] ?>"
                                   class="d-block">
                                    <img src="<?= getPicturePath(
                                        $picture['id'],
                                        $picture['name']
                                    ) ?>"
                                         class="img-fluid img-thumbnail"
                                         alt="<?= $picture['description'] ?>">
                                </a>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
        crossorigin="anonymous"></script>
</body>
</html>
