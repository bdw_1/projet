<?php

require_once 'header.php';

if (!empty($_POST)) {
    $connection = getConnection();

    $pseudo = $_POST['pseudo'] ?? null;
    $password = $_POST['password'] ?? null;

    $errors = checkUserForm($connection, $pseudo, $password);

    $passwordConfirmation = $_POST['password-confirmation'] ?? null;
    if ($password !== $passwordConfirmation) {
        $errors[] = "Vos 2 mots de passe ne correspondent pas";
    }

    if (empty($errors)) {
        // Tous les champs sont bons
        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

        $id = registerUser($connection, $pseudo, $hashedPassword);
        connectUser($id); // On connecte l'utilisateur

        closeConnexion($connection);

        setFlash("Votre compté a bien été crée");
        header('Location: index.php'); // Redirection à l'accueil
    }

    closeConnexion($connection);
}
?>

<html lang="fr">
<head>
    <meta charset="utf-8">

    <title>Pinterest - Inscription</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
          crossorigin="anonymous">
</head>

<body>
<div class="container">
    <?php
    $currentPage = "Registration";
    include 'nav.php';
    ?>

    <h1>Inscription</h1>

    <?php
    if (isset($errors)) {
        printFormErrors($errors);
    }
    ?>

    <form action="registration.php" method="post">
        <div class="form-floating mb-3">
            <input id="pseudo" name="pseudo" type="text" class="form-control">
            <label for="pseudo" class="form-label">Pseudo</label>
        </div>

        <div class="form-floating mb-3">
            <input id="password" name="password" type="password"
                   class="form-control">
            <label for="password" class="form-label">Mot de passe</label>
        </div>

        <div class="form-floating mb-3">
            <input id="password-confirmation" name="password-confirmation"
                   type="password" class="form-control">
            <label for="password-confirmation" class="form-label">Confirmation
                du mot de passe</label>
        </div>

        <button type="submit" class="btn btn-primary">S'inscrire</button>
    </form>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
        crossorigin="anonymous"></script>
</body>
</html>