<?php

require_once 'header.php';

$connection = getConnection();

if (!isLogged() || !isSessionAdmin($connection)) {
    header(
        "Location: index.php"
    ); // Si l'utilisateur n'est pas connecté ou que l'utilisateur n'est pas admin, on le redirige vers la page d'accueil
}

$users = getUsers($connection);

closeConnexion($connection);
?>

<html lang="fr">
<head>
    <meta charset="utf-8">

    <title>Pinterest - Liste des utilisateurs</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
          crossorigin="anonymous">

    <style>
        .clickable-row {
            cursor: pointer;
        }
    </style>
</head>

<body>
<div class="container">
    <?php
    $currentPage = "Users";
    include 'nav.php';
    ?>

    <h1>Liste des utilisateurs</h1>

    <?php
    printFlash();
    ?>

    <table class="table table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Pseudo</th>
            <th>Mot de passe (Hashé)</th>
            <th>Administrateur</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($users as $user) {
            ?>
            <tr class="clickable-row" onclick="<?php
            echo "window.location='editUser.php?id=${user['id']}'";
            ?>">
                <td><?= $user['id'] ?></td>
                <td><?= $user['pseudo'] ?></td>
                <td><?= $user['password'] ?></td>
                <td><?= $user['root'] ? "Oui" : "Non" ?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
        crossorigin="anonymous"></script>
</body>
</html>
