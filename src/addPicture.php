<?php

require_once 'header.php';

const KO = 1024;

if (!isLogged()) {
    header(
        'Location: index.php'
    ); // Si l'utilisateur n'est pas connecté, on le redirige vers la page d'accueil
}

function printCategory(string $prefix, string $name)
{
    ?>
    <option value="<?= $prefix ?>"><?= $name ?></option>
    <?php
}

$connection = getConnection();

$categories = getCategories($connection);

if (!empty($_POST)) {
    $file = $_FILES['file'] ?? null;
    $fileName = $file['name'];
    $fileExtension = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));

    if (!$file) {
        $errors[] = "Vous devez choisir un fichier";
    } else {
        $allowedFileExtensions = ["jpg", "jpeg", "gif", "png"];

        if (!in_array($fileExtension, $allowedFileExtensions)) {
            $errors[] = "Le fichier doit être une photo";
        } else {
            $fileSize = $file['size'];

            if ($fileSize > 100 * KO) {
                $errors[] = "La taille de votre fichier ne doit pas exceder 100ko";
            }
        }
    }

    $pictureDescription = $_POST['description'] ?? null;
    $pictureCategoryId = $_POST['category'] ?? null;

    $errors = array_merge(
        $errors ?? [],
        checkPictureForm(
            $connection,
            $pictureDescription,
            $pictureCategoryId
        )
    );

    if (empty($errors)) {
        // Tous les champs sont bons
        $pictureId = registerPicture(
            $connection,
            $fileName,
            $pictureDescription,
            $pictureCategoryId,
            getSessionId()
        ); // Ajout de la photo dans la base de données

        $targetPath = getPicturePath($pictureId, $fileName);
        move_uploaded_file(
            $file['tmp_name'],
            $targetPath
        ); // Déplacement dans le dossier pictures

        closeConnexion($connection);

        setFlash("L'image a bien été ajoutée");
        header(
            "Location: details.php?id=$pictureId"
        ); // Redirection aux détails de la photo
    }
}

closeConnexion($connection);
?>

<html lang="fr">
<head>
    <meta charset="utf-8">

    <title>Pinterest - Ajout d'une photo</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
          crossorigin="anonymous">
</head>

<body>
<div class="container">
    <?php
    $currentPage = "AddPicture";
    include 'nav.php';
    ?>

    <h1>Ajouter une photo</h1>

    <?php
    if (isset($errors)) {
        printFormErrors($errors);
    }
    ?>

    <form action="addPicture.php" method="post" enctype="multipart/form-data">
        <div class="mb-3">
            <input id="file" name="file" type="file" class="form-control">
        </div>

        <div class="form-floating mb-3">
            <textarea id="description" name="description"
                      class="form-control"></textarea>
            <label for="description" class="form-label">Description</label>
        </div>

        <div class="form-floating mb-3">
            <select id="category" name="category" class="form-select">
                <?php
                foreach ($categories as $category) {
                    printCategory($category['id'], $category['name']);
                }
                ?>
            </select>

            <label for="category">Catégorie</label>
        </div>

        <button type="submit" class="btn btn-primary">Ajouter</button>
    </form>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
        crossorigin="anonymous"></script>
</body>
</html>
