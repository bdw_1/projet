<?php

require_once 'header.php';

const ALL_CATEGORIES_PREFIX = "All";

function printCategory($prefix, $name)
{
    ?>
    <option value="<?= $prefix ?>"<?php
    global $selectedCategoryPrefix;
    if ($prefix == $selectedCategoryPrefix) {
        echo " selected";
    }
    ?>><?= $name ?></option>
    <?php
}

$selectedCategoryPrefix = $_GET['category'] ?? ALL_CATEGORIES_PREFIX;

$connection = getConnection();

$categories = getCategories($connection);

if ($selectedCategoryPrefix == ALL_CATEGORIES_PREFIX) {
    $pictures = getVisiblePictures($connection);
} else {
    $pictures = getVisiblePicturesByCategory(
        $connection,
        $selectedCategoryPrefix
    );
    $selectedCategory = getCategoryByName($connection, $selectedCategoryPrefix);
}

closeConnexion($connection);
?>

<html lang="fr">
<head>
    <meta charset="utf-8">

    <title>Pinterest - Accueil</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
          crossorigin="anonymous">
</head>

<body>

<div class="container">
    <?php
    printFlash();

    $currentPage = "Index";
    include 'nav.php';
    ?>

    <h1><?= isset($selectedCategory) ? "Photos de la catégorie {$selectedCategory['name']}" : "Toutes les photos" ?></h1>

    <form action="index.php" method="get">
        <div class="form-floating mb-3">
            <select id="category" name="category" class="form-select">
                <?php
                printCategory(ALL_CATEGORIES_PREFIX, "Toutes les catégories");

                foreach ($categories as $category) {
                    $categoryName = $category['name'];
                    printCategory($categoryName, $categoryName);
                }
                ?>
            </select>

            <label for="category">Photos à afficher</label>
        </div>

        <button type="submit" class="btn btn-primary">Afficher</button>
    </form>

    <p><?= count($pictures) ?> photo(s) sélectionnée(s)</p>

    <div class="row">
        <?php
        foreach ($pictures as $picture) {
            ?>
            <div class="col-lg-3 col-md-4 col-6">
                <a href="details.php?id=<?= $picture['id'] ?>" class="d-block">
                    <img src="<?= getPicturePath(
                        $picture['id'],
                        $picture['name']
                    ) ?>" class="img-fluid img-thumbnail"
                         alt="<?= $picture['description'] ?>">
                </a>
            </div>
            <?php
        }
        ?>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
        crossorigin="anonymous"></script>
</body>
</html>