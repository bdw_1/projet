<?php

require_once 'header.php';

$userId = $_GET['id'] ?? null;

$connection = getConnection();

if (!$userId || !isLogged() || !isSessionAdmin($connection)) {
    closeConnexion($connection);
    header("Location: index.php");
}

if ($userId == getSessionId()) {
    header('Location: profile.php');
}

$subPage = $_GET['page'] ?? "credentials";

$pictures = getPicturesFromUser($connection, $userId);

$currentUser = getUserById($connection, $userId);

$currentPseudo = $currentUser['pseudo'];
$currentPassword = $currentUser['password'];
$currentPrivileges = $currentUser['root'];

if (!empty($_POST)) {
    $newPseudo = $_POST['pseudo'] ?? null;
    $newPassword = $_POST['password'] ?? null;

    $errors = checkUserForm(
        $connection,
        $newPseudo,
        $newPassword,
        $currentPseudo
    );

    if (empty($errors)) {
        updateUserPseudo($connection, $userId, $newPseudo);
        $currentPseudo = $newPseudo;

        if ($newPassword !== $currentPassword) {
            $hashedPassword = password_hash($newPassword, PASSWORD_DEFAULT);
            updateUserPassword($connection, $userId, $hashedPassword);
            $currentPassword = $hashedPassword;
        }

        $newPrivileges = isset($_POST['root']) ? 1 : 0;
        updateUserPrivileges($connection, $userId, $newPrivileges);
        $currentPrivileges = $newPrivileges;

        setFlash("Les données de $newPseudo ont bien été sauvegardées");
    }
}

closeConnexion($connection);
?>

<html lang="fr">
<head>
    <meta charset="utf-8">

    <title>Pinterest - Édition d'un utilisateur</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
          crossorigin="anonymous">
</head>

<body>
<div class="container">
    <?php
    $currentPage = "EditUser";
    include 'nav.php';
    ?>

    <h1>Édition de <?= $currentPseudo ?></h1>

    <?php
    printFlash();
    ?>

    <?php
    if (isset($errors)) {
        printFormErrors($errors);
    }
    ?>

    <div class="mb-2">
        <a href="users.php">Revenir aux utilisateurs</a>
    </div>

    <div class="mt-3 d-flex align-items-start">
        <div class="nav flex-column nav-pills me-3" id="v-pills-tab"
             role="tablist" aria-orientation="vertical">
            <button class="nav-link<?php
            if ($subPage === "credentials") {
                echo " active";
            } ?>"
                    id="v-pills-credentials-tab" data-bs-toggle="pill"
                    data-bs-target="#v-pills-credentials" type="button"
                    role="tab" aria-controls="v-pills-credentials"
                    aria-selected="true">Identifiants
            </button>

            <button class="nav-link<?php
            if ($subPage === "photos") {
                echo " active";
            } ?>"
                    id="v-pills-pictures-tab" data-bs-toggle="pill"
                    data-bs-target="#v-pills-pictures"
                    type="button" role="tab" aria-controls="v-pills-pictures"
                    aria-selected="false">Ses photos
            </button>
        </div>
        <div class="tab-content" id="v-pills-tabContent">
            <div id="v-pills-credentials" class="tab-pane fade<?php
            if ($subPage === "credentials") {
                echo " show active";
            } ?>"
                 role="tabpanel"
                 aria-labelledby="v-pills-home-tab">
                <h2>Identifiants</h2>

                <form action="editUser.php?id=<?= $userId ?>" method="post">
                    <div class="form-floating mb-3">
                        <input id="pseudo" name="pseudo" type="text"
                               value="<?= $currentPseudo ?>"
                               class="form-control">
                        <label for="pseudo" class="form-label">Changer de
                            pseudo</label>
                    </div>

                    <div class="form-floating mb-3">
                        <input id="password" name="password" type="password"
                               class="form-control"
                               value="<?= $currentPassword ?>">
                        <label for="password" class="form-label">Changer de mot
                            de passe</label>
                        <div id="passwordHelp" class="form-text">Une
                            version cryptée du mot de passe de <?=
                            $currentPseudo ?> est
                            affichée</div>
                    </div>

                    <div class="mb-3 form-check form-switch">
                        <input id="switchRoot" name="root" type="checkbox"
                               class="form-check-input"<?php
                        if ($currentPrivileges) echo " checked" ?>>
                        <label for="switchRoot" class="form-check-label">Administrateur</label>
                    </div>

                    <button type="submit" class="btn btn-primary">Sauvegarder
                        cet utilisateur
                    </button>
                    <a class="btn btn-danger"
                       href="deleteUser.php?id=<?= $userId ?>" role="button">Supprimer
                        cet
                        utilisateur</a>
                </form>
            </div>

            <div id="v-pills-pictures" class="tab-pane fade<?php
            if ($subPage === "photos") {
                echo " show active";
            } ?>" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                <h3>Ses Photos</h3>

                <?php
                if (empty($pictures)) {
                    ?>
                    <p>Aucune photo à afficher</p>
                    <?php
                } else {
                    ?>
                    <div class="row">
                        <?php
                        foreach ($pictures as $picture) {
                            ?>
                            <div class="col-lg-3 col-md-4 col-6">
                                <a href="editPicture.php?id=<?= $picture['id'] ?>"
                                   class="d-block">
                                    <img src="<?= getPicturePath(
                                        $picture['id'],
                                        $picture['name']
                                    ) ?>"
                                         class="img-fluid img-thumbnail"
                                         alt="<?= $picture['description'] ?>">
                                </a>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
        crossorigin="anonymous"></script>
</body>
</html>