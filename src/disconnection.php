<?php

require_once 'header.php';

if (isLogged()) {
    disconnectUser();
}

header('Location: index.php');