<?php

require_once 'header.php';

$connection = getConnection();

$id = $_GET['id'] ?? null;

if (!$id || !isLogged() || (!isSessionAdmin($connection) && getOwnerId(
            $connection,
            $id
        ) != getSessionId())) {
    closeConnexion($connection);
    header("Location: index.php");
}

$name = getPictureName($connection, $id);
$filePath = getPicturePath($id, $name);

unlink($filePath); // On supprime la photo dans le dossier

$ownerId = getOwnerId($connection, $id);

deletePicture($connection, $id);

setFlash("La photo a bien été supprimée");

closeConnexion($connection);

if ($ownerId == getSessionId()) {
    // L'utilisateur édite ses propres photos
    header("Location: profile.php?page=photos");
} else {
    // L'utilisateur édite d'autres photos que les siennes
    header("Location: editUser.php?id=$ownerId&page=photos");
}