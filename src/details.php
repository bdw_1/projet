<?php

require_once 'header.php';

$pictureId = $_GET['id'] ?? null;

if (!$pictureId) {
    header('Location: index.php');
}

$connection = getConnection();

$picture = getPictureDetails($connection, $pictureId);

$pictureName = $picture['name'];
$pictureDescription = $picture['description'];
$pictureCategory = $picture['category'];

closeConnexion($connection);
?>

<html lang="fr">
<head>
    <meta charset="utf-8">

    <title>Pinterest - Détails</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
          crossorigin="anonymous">
</head>

<body>
<div class="container">
    <?php
    printFlash();

    $currentPage = "Index";
    include 'nav.php';
    ?>

    <h1>Détails de <?= $pictureName ?></h1>

    <div class="mb-2">
        <a href="index.php">Revenir à la liste des photos</a>
    </div>

    <?php
    printDivPictureImage($pictureId, $pictureName, $pictureDescription);
    ?>

    <table class="table">
        <tbody>
        <tr>
            <th>Nom du fichier</th>
            <td><?= $pictureName ?></td>
        </tr>
        <tr>
            <th>Description</th>
            <td><?= $pictureDescription ?></td>
        </tr>
        <tr>
            <th>Catégorie</th>
            <td>
                <a href="index.php?category=<?= $pictureCategory ?>"><?= $pictureCategory ?></a>
            </td>
        </tr>
        </tbody>
    </table>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
        crossorigin="anonymous"></script>
</body>
</html>