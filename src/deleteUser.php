<?php

require_once 'header.php';

$connection = getConnection();

$id = $_GET['id'] ?? null;

if (!$id || !isLogged() || !isSessionAdmin($connection)) {
    closeConnexion($connection);
    header("Location: index.php");
}

deleteUser($connection, $id);

closeConnexion($connection);