<?php

/**
 * Vérifie que le formulaire d'une édition d'utilisateur soit bien valide et
 * renvoie les erreurs éventuelles
 *
 * @param mysqli $connection connexion à la bdd
 * @param string|null $pseudo pseudo saisi
 * @param string|null $password mot de passe saisi
 * @param string|null $currentPseudo pseudo actuel de l'utilisateur édité
 * @return array tableau d'erreurs si il y a des erreurs, tableau vide sinon
 */
function checkUserForm(
    mysqli $connection,
    ?string $pseudo,
    ?string $password,
    string $currentPseudo = null
): array {
    $errors = [];

    $pseudoRegex = "#[a-zA-Z][a-zA-Z0-9]{2,}#";

    if (!$pseudo) {
        $errors[] = "Vous devez saisir un pseudo";
    } else {
        if ($pseudo !== $currentPseudo) {
            if (!preg_match($pseudoRegex, $pseudo)) {
                $errors[] = "Votre pseudo doit commencer par une lettre et comporter au moins 2 autres caractères alphanumériques";
            } else {
                if (pseudoExists($connection, $pseudo)) {
                    $errors[] = "Le pseudo que vous avez saisi existe déjà";
                }
            }
        }
    }

    $passwordRegex = "#.{3,}#";

    if (!$password) {
        $errors[] = "Vous devez saisir un mot de passe";
    } else {
        if (!preg_match($passwordRegex, $password)) {
            $errors[] = "Votre mot de passe doit comporter au moins 3 caractères";
        }
    }

    return $errors;
}

/**
 * Vérifie que le formulaire de l'édition d'une image soit bien valide et
 * renvoie les erreurs éventuelles.
 *
 * @param mysqli $connection connexion à la bdd
 * @param string|null $pictureDescription description de l'image saisie
 * @param int|null $pictureCategoryId identifiant de la catégorie
 * @return array tableau d'erreurs si il y a des erreurs, tableau vide sinon
 */
function checkPictureForm(
    mysqli $connection,
    ?string $pictureDescription,
    ?int $pictureCategoryId
): array {
    $errors = [];

    if (!$pictureDescription) {
        $errors[] = "Vous devez saisir une description";
    }

    if (!$pictureCategoryId) {
        $errors[] = "Vous devez choisir une catégorie";
    } else {
        if (!categoryExists($connection, $pictureCategoryId)) {
            $errors[] = "Cette catégorie n'existe pas";
        }
    }

    return $errors;
}

/**
 * Affiche une notification spécifiant qu'il y a une erreur dans un formulaire
 *
 * @param string $error erreur à afficher
 */
function printFormError(string $error)
{
    ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <?= $error ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert"
                aria-label="Close"></button>
    </div>
    <?php
}

/**
 * Affiche toutes les erreurs au sein d'un formulaire
 *
 * @param array $errors tableau des erreurs
 */
function printFormErrors(array $errors)
{
    foreach ($errors as $error) {
        printFormError($error);
    }
}