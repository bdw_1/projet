<?php

const DB_HOST = 'localhost';
const DB_USERNAME = 'root';
const DB_PASSWORD = 'root';
const DB_NAME = 'pinterest';
const DB_PORT = 8889;

/**
 * Renvoie la connexion à la bdd
 *
 * @return mysqli connexion àa la bdd
 */
function getConnection(): mysqli
{
    $connection = mysqli_connect(
        DB_HOST,
        DB_USERNAME,
        DB_PASSWORD,
        DB_NAME,
        DB_PORT
    );

    if (!$connection) {
        echo "Echec lors de la connexion a la base de donnees : (" . mysqli_connect_errno(
            ) . ") " . mysqli_connect_error();
        exit(-1);
    }

    return $connection;
}

/**
 * Execute la requête sql passée en paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param string $query requête sql (SELECT)
 * @return bool|mysqli_result résultat de la requête si la requête est
 * valide, "faux" sinon.
 */
function executeQuery(mysqli $connection, string $query)
{
    $result = mysqli_query($connection, $query);

    if (!$result) {
        echo "$query comporte une erreur de syntaxe";
    }

    return $result;
}

/**
 * Execute la requête sql de mise à jour passée en paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param string $query requête sql de mise à jour (UPDATE, INSERT,
 * DELETE)
 */
function executeUpdate(mysqli $connection, string $query)
{
    $result = mysqli_query($connection, $query);

    if (!$result) {
        echo "$query comporte une erreur de syntaxe";
    }
}

/**
 * Transforme le résultat de la requête passée en paramètre en un tableau
 * php
 *
 * @param mysqli_result $result résultat d'une requête sql
 * @return array tableau associatif des lignes du résultat de la requête
 */
function fetchResult(mysqli_result $result): array
{
    $array = [];
    $index = 0;

    while ($row = mysqli_fetch_assoc($result)) {
        $array[$index] = $row;

        $index++;
    }

    return $array;
}

/**
 * Ferme la connexion à la bdd passée en paramètre
 *
 * @param mysqli $connection connexion à la bdd
 */
function closeConnexion(mysqli $connection)
{
    mysqli_close($connection);
}