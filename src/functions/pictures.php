<?php

/**
 * Récupère l'identifiant du propriétaire de l'image dont l'identifiant est
 * passé en paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param int $id identifiant de l'image
 * @return int identifiant du propriétaire de l'image
 */
function getOwnerId(mysqli $connection, int $id): int
{
    $query = "SELECT owner_id FROM pictures WHERE id = $id";
    $result = executeQuery($connection, $query);

    return mysqli_fetch_assoc($result)['owner_id'];
}

/**
 * Récupère le nom de l'image dont l'identifiant est passé en paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param int $id identifiant de l'image
 * @return string nom de l'image
 */
function getPictureName(mysqli $connection, int $id): string
{
    $query = "SELECT file_name AS name FROM pictures WHERE id = $id";
    $result = executeQuery($connection, $query);

    return mysqli_fetch_assoc($result)['name'];
}

/**
 * Récupère les propriétés d'une image dont l'identifiant est passé en
 * paramètre (à afficher pour les détails d'une image)
 *
 * @param mysqli $connection connexion à la bdd
 * @param int $id identifiant de l'image
 * @return array tableau associatif des propriétés de l'image
 */
function getPictureDetails(mysqli $connection, int $id): array
{
    $query = "SELECT file_name AS name, description, name AS category FROM pictures p JOIN categories c on c.id = category_id WHERE p.id = $id";
    $result = executeQuery($connection, $query);

    return mysqli_fetch_assoc($result);
}

/**
 * Récupère les propriétés d'une image dont l'identifiant est passé en
 * paramètre (à afficher pour l'édition d'une image)
 *
 * @param mysqli $connection connexion à la bdd
 * @param int $id identifiant de l'image
 * @return array tableau associatif des propriétés de l'image
 */
function getUserPictureDetails(mysqli $connection, int $id): ?array
{
    $query = "SELECT file_name AS name, description, is_visible AS isVisible, c.id AS category FROM pictures p JOIN categories c on c.id = category_id WHERE p.id = $id";
    $result = executeQuery($connection, $query);

    return mysqli_fetch_assoc($result);
}

/**
 * Récupère toutes les images visibles
 *
 * @param mysqli $connection connexion à la bdd
 * @return array tableau d'images contenant ses propriétés
 */
function getVisiblePictures(mysqli $connection): array
{
    $query = "SELECT id, file_name AS name, description FROM pictures WHERE is_visible = 1";
    $result = executeQuery($connection, $query);

    return fetchResult($result);
}

/**
 * Récupère toutes les images visibles contenues dans la catégorie dont le nom
 * est passé en paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param string $categoryName nom de la catégorie
 * @return array tableau d'images contenant ses propriétés
 */
function getVisiblePicturesByCategory(
    mysqli $connection,
    string $categoryName
): array {
    $query = "SELECT p.id, file_name AS `name`, description FROM pictures p JOIN categories c ON c.id = category_id WHERE name = '$categoryName' AND is_visible = 1";
    $result = executeQuery($connection, $query);

    return fetchResult($result);
}

/**
 * Récupère le nombre d'images par catégorie
 *
 * @param mysqli $connection connexion à la bdd
 * @return array tableau de catégories avec pour valeur un tableau associatif
 * contenant le nom de la catégorie et le nombre d'images dans cette catégorie
 */
function getPicturesByCategory(mysqli $connection): array
{
    $query = "SELECT c.name AS category, COUNT(p.id) AS count FROM pictures p JOIN categories c on c.id = p.category_id GROUP BY category_id";
    $result = executeQuery($connection, $query);

    return fetchResult($result);
}

/**
 * Récupère les images possédées par le propriétaire dont l'identifiant est
 * passé en paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param int $ownerId identifiant du propriétaire
 * @return array tableau des images du propriétaire avec pour valeur les
 * propriétés de l'image
 */
function getPicturesFromUser(mysqli $connection, int $ownerId): array
{
    $query = "SELECT p.id, file_name AS `name`, description, is_visible FROM pictures p JOIN categories c ON c.id = category_id WHERE owner_id = $ownerId";
    $result = executeQuery($connection, $query);

    return fetchResult($result);
}

/**
 * Récupère le nombre d'images par utilisateur
 *
 * @param mysqli $connection connexion à la bdd
 * @return array tableau des utilisateurs dans lequel chaque valeur est un
 * tableau associatif contenant les propriétés de l'utilisateur et son nombre
 * d'images
 */
function getPicturesOrderedByUser(mysqli $connection): array
{
    $query = "SELECT COUNT(p.id) AS count, u.id AS userId, u.pseudo AS userPseudo FROM pictures p JOIN users u on u.id = p.owner_id GROUP BY u.pseudo";
    $result = executeQuery($connection, $query);

    return fetchResult($result);
}

/**
 * Met à jour le nom de l'image dont l'identifiant est passé en paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param int $id identifiant de l'image
 * @param string $newName nouveau nom de l'image
 */
function updatePictureName(mysqli $connection, int $id, string $newName)
{
    $query = "UPDATE pictures SET file_name = '$newName' WHERE id = $id";
    executeUpdate($connection, $query);
}

/**
 * Met à jour la description de l'image dont l'identifiant est passé en
 * paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param int $id identifiant de l'image
 * @param string $newDescription nouvelle description de l'image
 */
function updatePictureDescription(
    mysqli $connection,
    int $id,
    string $newDescription
) {
    $query = "UPDATE pictures SET description = '$newDescription' WHERE id = $id";
    executeUpdate($connection, $query);
}

/**
 * Met à jour la catégorie de l'image dont l'identifiant est passé en paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param int $id identifiant de l'image
 * @param int $categoryId identifiant de la nouvelle catégorie de l'image
 */
function updatePictureCategory(mysqli $connection, int $id, int $categoryId)
{
    $query = "UPDATE pictures SET category_id = $categoryId WHERE id = $id";
    executeUpdate($connection, $query);
}

/**
 * Met à jour la visibilité de l'image dont l'identifiant est passé en paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param int $id identifiant de l'image
 * @param int $isVisible nouvelle visibilité (1 = visible, 0 = non visible)
 */
function updatePictureVisibility(mysqli $connection, int $id, int $isVisible)
{
    $query = "UPDATE pictures SET is_visible = $isVisible WHERE id = $id";
    executeUpdate($connection, $query);
}

/**
 * Enregistre une nouvelle image et retourne l'identifiant de l'image insérée
 *
 * @param mysqli $connection connexion à la bdd
 * @param string $name nom de l'image
 * @param string $description description de l'image
 * @param int $categoryId identifiant de la catégorie de l'image
 * @param int $ownerId identifiant du propriétaire de l'image
 * @return int identifiant de l'image insérée
 */
function registerPicture(
    mysqli $connection,
    string $name,
    string $description,
    int $categoryId,
    int $ownerId
): int {
    $query = "INSERT INTO pictures(file_name, description, category_id, owner_id) VALUES ('$name', '$description', $categoryId, $ownerId)";
    executeUpdate($connection, $query);

    return mysqli_insert_id($connection);
}

/**
 * Supprime l'utilisateur dont l'identifiant est passé en paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param int $id identifiant de l'utilisateur
 */
function deletePicture(mysqli $connection, int $id)
{
    $query = "DELETE FROM pictures WHERE id = $id";
    executeUpdate($connection, $query);
}