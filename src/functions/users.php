<?php

/**
 * Vérifie que le pseudo passé en paramètre existe
 *
 * @param mysqli $connection connexion à la bdd
 * @param string $pseudo pseudo
 * @return bool vrai si le pseudo existe, faux sinon
 */
function pseudoExists(mysqli $connection, string $pseudo): bool
{
    $query = "SELECT 1 FROM users WHERE pseudo = '$pseudo'";
    $result = executeQuery($connection, $query);

    return mysqli_num_rows($result) == 1;
}

/**
 * Récupère les infos de l'utilisateur dont l'identifiant est passé en
 * paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param int $id identifiant de l'utilisateur
 * @return array infos de l'utilisateur sous forme de tableau associatif
 */
function getUserById(mysqli $connection, int $id): ?array
{
    $query = "SELECT pseudo, password, root FROM users WHERE id = $id";
    $result = executeQuery($connection, $query);

    return mysqli_fetch_assoc($result);
}

/**
 * Récupère les infos de l'utilisateur dont le pseudo est passé en paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param string $pseudo pseudo de l'utilisateur
 * @return array infos de l'utilisateur sous forme d'un tableau associatif
 */
function getUserByPseudo(mysqli $connection, string $pseudo): ?array
{
    $query = "SELECT id, password, root FROM users WHERE pseudo = '$pseudo'";
    $result = executeQuery($connection, $query);

    return mysqli_fetch_assoc($result);
}

/**
 * Récupère tous les utilisateurs
 *
 * @param mysqli $connection connexion à la bdd
 * @return array tableau des utilisateurs ou chaque valeur est un tableau
 * associatif des infos de l'utilisateur pour l'utilisateur en question
 */
function getUsers(mysqli $connection): array
{
    $query = "SELECT id, pseudo, password, root FROM users";
    $result = executeQuery($connection, $query);

    return fetchResult($result);
}

/**
 * Met à jour le mot de passe de l'utilisateur dont l'identifiant est passé
 * en paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param int $id identifiant de l'utilisateur
 * @param string $newPassword nouveau mot de passe de l'utilisateur
 */
function updateUserPassword(mysqli $connection, int $id, string $newPassword)
{
    $query = "UPDATE users SET password = '$newPassword' WHERE id = $id";
    executeUpdate($connection, $query);
}

/**
 * Met à jour le pseudo de l'utilisateur dont l'identifiant est passé en
 * paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param int $id identifiant de l'utilisateur
 * @param string $newPseudo nouveau pseudo de l'utilisateur
 */
function updateUserPseudo(mysqli $connection, int $id, string $newPseudo)
{
    $query = "UPDATE users SET pseudo = '$newPseudo' WHERE id = $id";
    executeUpdate($connection, $query);
}

/**
 * Met à jour les priviliges de l'utilisateur dont l'identifiant est passé en
 * paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param int $id identifiant de l'utilisateur
 * @param int $privileges nouveaux privileges (1 = Admin, 0 = Non admin)
 */
function updateUserPrivileges(mysqli $connection, int $id, int $privileges)
{
    $query = "UPDATE users SET root = $privileges WHERE id = $id";
    executeUpdate($connection, $query);
}

/**
 * Enregistre un nouvel utilisateur et retourne l'identifiant de
 * l'utilisateur inséré
 *
 * @param mysqli $connection connexion à la bdd
 * @param string $pseudo pseudo de l'utilisateur
 * @param string $password mot de passe de l'utilisateur
 * @return int identifiant de l'utilisateur inséré
 */
function registerUser(mysqli $connection, string $pseudo, string $password): int
{
    $query = "INSERT INTO users(pseudo, password) VALUES ('$pseudo', '$password')";
    executeUpdate($connection, $query);

    return mysqli_insert_id($connection);
}

/**
 * Supprime l'utilisateur dont l'identifiant est passé en paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param int $id identifiant de l'utilisateur
 */
function deleteUser(mysqli $connection, int $id)
{
    $query = "DELETE FROM users WHERE id = $id";
    executeUpdate($connection, $query);
}