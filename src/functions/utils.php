<?php

/**
 * Debug la variable passé en paramètre en affichant ses détails
 *
 * @param mixed $var variable à débug
 */
function debug($var)
{
    ?>
    <pre>
        <?php
        var_dump($var);
        ?>
    </pre>
    <?php
}

/**
 * Affiche la balise image dont les caractéristiques sont passées en paramètre
 *
 * @param int $pictureId identifiant de l'image
 * @param string $pictureName nom de l'image
 * @param string $pictureDescription description de l'image
 */
function printDivPictureImage(
    int $pictureId,
    string $pictureName,
    string $pictureDescription
) {
    ?>
    <div class="col-lg-3 col-md-4 col-6">
        <img src="<?= getPicturePath($pictureId, $pictureName) ?>"
             class="img-fluid img-thumbnail"
             alt="<?= $pictureDescription ?>"
    </div>
    <?php
}

/**
 * Extrait et renvoie l'extension du fichier dont le nom est passé en paramètre
 *
 * @param string $fileName nom du fichier
 * @return string extension du fichier
 */
function getPictureExtension(string $fileName): string
{
    $info = pathinfo($fileName, PATHINFO_EXTENSION);

    return strtolower($info);
}

/**
 * Renvoie le chemin d'accès dans le dossier pictures de l'image dont
 * l'identifiant et dont le nom sont passés en paramètre
 *
 * @param int $id identifiant de l'image
 * @param string $fileName nom du fichier
 * @return string chemin d'accès dans le dossier pictures de l'image
 */
function getPicturePath(int $id, string $fileName): string
{
    $fileExtension = getPictureExtension($fileName);

    return "../pictures/DSC_$id.$fileExtension";
}

/**
 * Renvoie le nom d'un fichier sans son extension
 *
 * @param string $fileName nom du fichier (avec l'extension)
 * @return string nom du fichier sans l'extension
 */
function getPictureBaseName(string $fileName): string
{
    $fileExtension = getPictureExtension($fileName);

    return basename($fileName, "." . $fileExtension);
}