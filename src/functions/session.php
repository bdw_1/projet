<?php

/**
 * Retourne l'identifiant de l'utilisateur lié à la session ouverte
 *
 * @return int identifiant de l'utilisateur de la session
 */
function getSessionId(): int
{
    return $_SESSION['id'];
}

/**
 * Retourne les propriétés de l'utilisateur lié à la session ouverte
 *
 * @param mysqli $connection connexion à la bdd
 * @return array tableau de propriétés de l'utilisateur de la session
 */
function getSessionUser(mysqli $connection): ?array
{
    return getUserById($connection, getSessionId());
}

/**
 * Retourne le pseudo de l'utilisateur lié à la session ouverte
 *
 * @param mysqli $connection connexion à la bdd
 * @return string pseudo de l'utilisateur de la session
 */
function getSessionPseudo(mysqli $connection): string
{
    return getSessionUser($connection)['pseudo'];
}

/**
 * Retourne le mot de passe de l'utilisateur lié à la session ouverte
 *
 * @param mysqli $connection connexion
 * @return string mot de passe de l'utilisateur de la session
 */
function getSessionPassword(mysqli $connection): string
{
    return getSessionUser($connection)['password'];
}

/**
 * Vérifie que l'utilisateur lié à la session ouverte est administrateur
 *
 * @param mysqli $connection connexion à la bdd
 * @return bool vrai si l'utilisateur de la session est admin, faux sinon
 */
function isSessionAdmin(mysqli $connection): bool
{
    return getSessionUser($connection)['root'];
}

/**
 * Vérifie que la session est ouverte
 *
 * @return bool vrai si la session est ouverte, faux sinon
 */
function isLogged(): bool
{
    return isset($_SESSION['logged']) && $_SESSION['logged'];
}

/**
 * Connecte l'utilisateur dont l'identifiant est passé en paramètre à la session
 *
 * @param int $id identifiant de l'utilisateur
 */
function connectUser(int $id)
{
    $_SESSION['logged'] = true;
    $_SESSION['id'] = $id;
    $_SESSION['openTime'] = time();
}

/**
 * Déconnecte l'utilisateur de la session
 */
function disconnectUser()
{
    session_destroy();
    session_unset();
}

/**
 * Change le message flash lié à la session ouverte
 *
 * @param string $flash message flash
 */
function setFlash(string $flash)
{
    $_SESSION['flash'] = $flash;
}

/**
 * Affiche le message flash liée à la session ouverte
 */
function printFlash()
{
    $flash = $_SESSION['flash'] ?? null;

    if ($flash) {
        ?>
        <div class="mt-3 alert alert-success alert-dismissible" role="alert">
            <?= $flash ?>
            <button type="button" class="btn-close" data-bs-dismiss="alert"
                    aria-label="Close"></button>
        </div>
        <?php

        unset($_SESSION['flash']);
    }
}