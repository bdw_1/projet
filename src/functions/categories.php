<?php

/**
 * Vérifie que la catégorie dont l'id est passé en paramètre existe
 *
 * @param mysqli $connection connection à la bdd
 * @param int $id id de la catégorie
 * @return bool vrai si la catégorie existe, faux sinon
 */
function categoryExists(mysqli $connection, int $id): bool
{
    $query = "SELECT 1 FROM categories WHERE id = {$id}";
    $result = executeQuery($connection, $query);

    return mysqli_num_rows($result) == 1;
}

/**
 * Retourne l'ensemble des catégories sous la forme d'un tableau associatif
 *
 * @param mysqli $connection connexion à la badd
 * @return array tableau associatif avec pour clé l'id de la catégorie et pour
 * valeur un tableau associatif contenant ses propriétés (id, nom)
 */
function getCategories(mysqli $connection): array
{
    $categories = [];
    $index = 0;

    $query = "SELECT * FROM categories";
    $result = executeQuery($connection, $query);

    while ($row = mysqli_fetch_assoc($result)) {
        $categories[$row['id']] = $row;
        $index++;
    }

    return $categories;
}

/**
 * Récupère la catégorie dont le nom est passé en paramètre
 *
 * @param mysqli $connection connexion à la bdd
 * @param string $name nom de la catégorie
 * @return array|null tableau associatif contenant les propriétés de
 * la catégorie dont le nom est passé en paramètre (id, nom).
 */
function getCategoryByName(mysqli $connection, string $name): ?array
{
    $query = "SELECT * FROM categories WHERE name = '{$name}'";
    $result = executeQuery($connection, $query);

    return mysqli_fetch_assoc($result);
}