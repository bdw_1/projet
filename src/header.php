<?php

// On reporte toutes les erreurs
error_reporting(E_ALL);
ini_set("display_errors", 1);

if (!session_id()) {
    session_start();
}

require_once 'functions/utils.php';
require_once 'functions/forms.php';

require_once 'functions/database.php';

require_once 'functions/categories.php';
require_once 'functions/pictures.php';
require_once 'functions/users.php';

require_once 'functions/session.php';