<?php

require_once 'header.php';

$pictureId = $_GET['id'] ?? null;

if (!$pictureId || !isLogged()) {
    header('Location: index.php');
}

function printCategory($prefix, $name)
{
    ?>
    <option value="<?= $prefix ?>"<?php
    global $currentPictureCategoryId;
    if ($prefix == $currentPictureCategoryId) {
        echo " selected";
    }
    ?>><?= $name ?></option>
    <?php
}

$connection = getConnection();

$ownerId = getOwnerId($connection, $pictureId);

$categories = getCategories($connection);

$picture = getUserPictureDetails($connection, $pictureId);

$currentPictureName = $picture['name'];
$currentPictureDescription = $picture['description'];
$currentPictureCategoryId = $picture['category'];
$isPictureCurrentlyVisible = $picture['isVisible'];

if (!empty($_POST)) {
    $newPictureName = $_POST['file-name'] ?? null;

    if (!$newPictureName) {
        $errors[] = "Vous devez saisir un nom";
    }

    $newPictureDescription = $_POST['description'] ?? null;
    $newPictureCategoryId = $_POST['category'] ?? null;

    $errors = array_merge(
        $errors ?? [],
        checkPictureForm(
            $connection,
            $newPictureDescription,
            $newPictureCategoryId
        )
    );

    if (empty($errors)) {
        // Tous les champs sont bons
        $newPictureName .= "." . getPictureExtension($currentPictureName);

        updatePictureName($connection, $pictureId, $newPictureName);
        $currentPictureName = $newPictureName;

        updatePictureDescription(
            $connection,
            $pictureId,
            $newPictureDescription
        );
        $currentPictureDescription = $newPictureDescription;

        updatePictureCategory($connection, $pictureId, $newPictureCategoryId);
        $currentPictureCategoryId = $newPictureCategoryId;

        $isPictureNowVisible = isset($_POST['visible']) ? 1 : 0;
        updatePictureVisibility($connection, $pictureId, $isPictureNowVisible);
        $isPictureCurrentlyVisible = $isPictureNowVisible;

        setFlash("Vos modifications ont bien été prises en compte");
    }
}

$pictureBaseName = getPictureBaseName($currentPictureName);

closeConnexion($connection);
?>

<html lang="fr">
<head>
    <meta charset="utf-8">

    <title>Pinterest - Édition d'une photo</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
          crossorigin="anonymous">
</head>

<body>
<div class="container">
    <?php
    $currentPage = "EditPicture";
    include 'nav.php';
    ?>

    <h1>Édition de <?= $currentPictureName ?></h1>

    <?php
    printFlash();
    ?>

    <?php
    if (isset($errors)) {
        printFormErrors($errors);
    }
    ?>

    <div class="mb-2">
        <?php
        if ($ownerId == getSessionId()) {
            // L'utilisateur édite ses propres photos
            ?>
            <a href="profile.php?page=photos">Revenir à mes photos</a>
            <?php
        } else {
            // L'utilisateur édite d'autres photos que les siennes
            ?>
            <a href="editUser.php?id=<?= $ownerId ?>&page=photos">Revenir à ses
                photos</a>
            <?php
        }
        ?>
    </div>

    <form action="editPicture.php?id=<?= $pictureId ?>" method="post">
        <?php
        printDivPictureImage(
            $pictureId,
            $currentPictureName,
            $currentPictureDescription
        );
        ?>

        <table class="table">
            <tbody>
            <tr>
                <th><label for="file-name">Nom du fichier</label></th>
                <td>
                    <input id="file-name" name="file-name" type="text"
                           value="<?= $pictureBaseName ?>"
                           class="form-control">
                </td>
            </tr>
            <tr>
                <th><label for="description">Description</label></th>
                <td>
                    <input id="description" name="description" type="text"
                           value="<?= $currentPictureDescription ?>"
                           class="form-control">
                </td>
            </tr>
            <tr>
                <th><label for="category">Catégorie</label></th>
                <td>
                    <select id="category" name="category" class="form-select">
                        <?php
                        foreach ($categories as $category) {
                            printCategory($category['id'], $category['name']);
                        }
                        ?>
                    </select>
                </td>
            </tr>
            </tbody>
        </table>

        <div class="mb-3 form-check form-switch">
            <input id="switchVisibility" name="visible" type="checkbox"
                   class="form-check-input"<?php
            if ($isPictureCurrentlyVisible) echo " checked" ?>>
            <label for="switchVisibility"
                   class="form-check-label">Visible</label>
        </div>

        <button type="submit" class="btn btn-primary">Sauvegarder</button>
        <a class="btn btn-danger" href="deletePicture.php?id=<?= $pictureId ?>"
           role="button">Supprimer</a>
    </form>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
        crossorigin="anonymous"></script>
</body>
</html>