-- Fichier tuplesPhoto.sql

CREATE TABLE IF NOT EXISTS `categories`
(
    `id`   int(11)      NOT NULL,
    `name` varchar(250) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT INTO `categories` (`id`, `name`)
VALUES (1, 'Animaux'),
       (2, 'Objets'),
       (3, 'Personnalités');

CREATE TABLE IF NOT EXISTS `pictures`
(
    `id`          int(11)      NOT NULL,
    `file_name`   varchar(250) NOT NULL,
    `description` varchar(250) NOT NULL,
    `category_id` int(11)      NOT NULL,
    `owner_id`    int(11)      NOT NULL,
    `is_visible`  tinyint(1)   NOT NULL DEFAULT '1'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `users`
(
    `id`       int(11)      NOT NULL,
    `pseudo`   varchar(250) NOT NULL,
    `password` varchar(250) NOT NULL,
    `root`     tinyint(1)   NOT NULL DEFAULT '0'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT INTO `users` (`id`, `pseudo`, `password`, `root`)
VALUES (1, 'Administrateur',
        '$2y$10$YXIZTuUr0v9lZqciI1ueLuBnFn1nniNuyQBSQw6SaWfU0k6iLGM/S', 1);


ALTER TABLE `categories`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `name` (`name`);

ALTER TABLE `pictures`
    ADD PRIMARY KEY (`id`),
    ADD KEY `category_id` (`category_id`),
    ADD KEY `pictures_ibfk_2` (`owner_id`);

ALTER TABLE `users`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `pseudo` (`pseudo`);

ALTER TABLE `pictures`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 11;

ALTER TABLE `users`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 4;

ALTER TABLE `pictures`
    ADD CONSTRAINT `pictures_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
    ADD CONSTRAINT `pictures_ibfk_2` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`);